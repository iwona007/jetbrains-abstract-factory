package pl.iwona.factorylaptop;

public interface Display {
   String toString();
}
