package pl.iwona.factorylaptop;


public class MackBookDetailsFactory implements LaptopDetailsFactory {
    @Override
    public Display createDisplay() {
        return new MackBookDisplay();
    }

    @Override
    public GraphicCard createGraphicCard() {
        return new MackBookGraphicCard();
    }

    @Override
    public Processor createProcessor() {
        return new MackBookProcessor();
    }

    @Override
    public SSD createSSD() {
        return new MackBookSSD();
    }
}
