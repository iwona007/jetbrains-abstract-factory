package pl.iwona.factorylaptop;

public class Main {
    public static void main(String[] args) throws InterruptedException {

        Laptop laptop;

        LaptopFactory dellFactory = new DellXPSFactory();
        LaptopFactory mackbookFactory = new MackBookFactory();

        System.out.println("-Hello, I need Windows laptop");
        System.out.println("-Okay! Please wait for a sec, - Calling to the DellXPSFactory. -Bring me the Dell XPS 9370");
        Thread.sleep(1500);
        laptop = dellFactory.createComputer();
        System.out.println(laptop.getDescription());
        System.out.println("There it is!");


        System.out.println("-Hello, I need MacOS laptop");
        System.out.println("-Okay! Please wait for a sec, - Calling to the MackBookFactory. -Bring me the MackBook Pro 13\"");
        Thread.sleep(1500);
        laptop = mackbookFactory.createComputer();
        System.out.println(laptop.getDescription());
        System.out.println("There it is!");

    }
}
