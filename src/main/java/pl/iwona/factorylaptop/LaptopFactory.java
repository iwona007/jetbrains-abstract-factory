package pl.iwona.factorylaptop;

public interface LaptopFactory {
    Laptop createComputer();
}
