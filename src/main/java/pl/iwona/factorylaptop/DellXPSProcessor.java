package pl.iwona.factorylaptop;

import pl.iwona.factorylaptop.Processor;

public class DellXPSProcessor implements Processor {
    @Override
    public String toString() {
        return "Core i7";
    }
}
