package pl.iwona.factorylaptop;

public interface Processor {
    String toString();
}
