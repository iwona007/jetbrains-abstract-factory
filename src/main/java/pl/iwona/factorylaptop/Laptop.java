package pl.iwona.factorylaptop;

import pl.iwona.factorylaptop.Display;

import pl.iwona.factorylaptop.GraphicCard;
import pl.iwona.factorylaptop.Processor;
import pl.iwona.factorylaptop.SSD;

public abstract class Laptop {
    Display display;
    GraphicCard graphicCard;
    Processor processor;
    SSD ssd;

    abstract String getDescription();

    @Override
    public String toString() {
        return "Display=" + display.toString() + "\n" +
                ", graphicCard=" + graphicCard.toString() + "\n" +
                ", processor=" + processor.toString() + "\n" +
                ", ssd=" + ssd.toString();
    }
}
