package pl.iwona.factorylaptop;

public class MackBook extends Laptop {

    public MackBook(LaptopDetailsFactory detailsFactory) {
        display = detailsFactory.createDisplay();
        graphicCard = detailsFactory.createGraphicCard();
        processor = detailsFactory.createProcessor();
        ssd = detailsFactory.createSSD();
    }

    @Override
    String getDescription() {
        return "This is THE MackBook Pro 13\"\n" + super.toString();
    }
}
