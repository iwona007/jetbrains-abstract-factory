package pl.iwona.factorylaptop;

public class MackBookFactory implements LaptopFactory {
    @Override
    public Laptop createComputer() {
        LaptopDetailsFactory detailsFactory = new MackBookDetailsFactory();
        return new MackBook(detailsFactory);
    }
}
