package pl.iwona.factorylaptop;

public class DellXPS extends Laptop {

    public DellXPS(LaptopDetailsFactory detailsFactory){
        display = detailsFactory.createDisplay();
        graphicCard = detailsFactory.createGraphicCard();
        processor = detailsFactory.createProcessor();
        ssd = detailsFactory.createSSD();
    }

    @Override
    String getDescription() {
        return "This is Dell XPS 9370\n" + super.toString();
    }
}
